"""setup URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from setup import settings
from posixpath import basename
from django.contrib import admin
from django.urls import path, include, re_path
from django.conf.urls.static import static
from Dashboard.views import CategoriaViewSet, list_games, GameList, TeamList
from game.views import GameViewSet, TeamViewSet, VersusViewSet
from Dashboard.viewsGame import NextGames
from Dashboard.viewsPlayer import PlayersRank, EquipeJogadorJogo, rank_players_championship
from Dashboard.viewsTeam import list_team_analytics, teams_list, players_list_at_team
from Dashboard.viewsNoticie import Noticies
from Dashboard.viewsChampionship import championship_menu

from django.conf import settings
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework import routers


router = routers.DefaultRouter()
router.register('games',GameViewSet,basename='Games')
router.register('teams',TeamViewSet,basename='Teams')
router.register('versus',VersusViewSet,basename='Versus')
router.register('categorias',CategoriaViewSet,basename='Categoria')


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/',include('rest_framework.urls',namespace='rest_framework')),
    path('api-token-auth/',obtain_auth_token,name="api_token_auth"),
    
    path('next-games',NextGames.as_view(),name='next_games'),
    path('players-rank',PlayersRank.as_view(),name='players_rank'),
    path('players-team-game',EquipeJogadorJogo.as_view(),name='players_team_game'),
    path('rank-players-championship',rank_players_championship,name='rank_players_championship'),

    path('noticies',Noticies.as_view(),name='noticie'),

    path('championship',championship_menu,name='championship_menu'),

    path('teams',teams_list,name='teams_list'),
    re_path(r'^teams/(?P<categoryid>\d+)/(?P<teamid>\d+)/$',players_list_at_team,name='players_list_at_team'),

    re_path(r'^games-analytics/(?P<categoriaid>[0-9]{2})/(?P<type>\w+)/(?P<date>[0-9]{4})/$',list_team_analytics,name='games_analytics'),
    
    
    path('',include(router.urls))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)