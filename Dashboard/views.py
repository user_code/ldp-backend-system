from datetime import datetime
from time import timezone
from typing import Any, Dict
from django.shortcuts import render
from rest_framework import routers, serializers, viewsets, generics
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated

from Dashboard.models import Categoria, Equipe
from Dashboard.serializer import CategoriaSerializer, EquipeSerializer
from django.http import JsonResponse, HttpResponse
from django.shortcuts import get_object_or_404
from django.views.generic.list import ListView

# Create your views here.
class CategoriaViewSet(viewsets.ModelViewSet):

    queryset = Categoria.objects.all()
    serializer_class = CategoriaSerializer
    permission_classes = (IsAuthenticated,)

def list_games(request):
    print(request)
    games = Equipe.objects.select_related('categoriaid')
    print(games)
    return JsonResponse({"categoria": "test"})

class GameList(ListView):

    model = Equipe
    paginate_by = 10

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        return context


class PaginationTeamList(PageNumberPagination):
    page_size = 5

class TeamList(generics.ListCreateAPIView):
    queryset = Equipe.objects.select_related('categoriaid')
    serializer_class = EquipeSerializer
    pagination_class = PaginationTeamList

    def get_object(self):
        queryset = self.queryset
        print(self.multiple_lookup_fields)
        obj = get_object_or_404(queryset)
        return obj
