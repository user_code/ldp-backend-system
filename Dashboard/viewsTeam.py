from asyncio.windows_events import NULL
from datetime import date, datetime
from genericpath import exists
from django.shortcuts import get_object_or_404
from django.db.models import Q, F, Func, Count, Sum, Avg
from Dashboard.models import Campeonato, Categoria, Equipe, Equipejogadorjogo, Jogo, Equipejogador
from Dashboard.serializer import EquipeSerializer, EquipejogadorSerializer, JogoSerializer, EquipejogadorjogoSerializer, EquipejogadorjogoBaseSerializer

from rest_framework import generics, viewsets, pagination, filters, decorators, response, parsers, status

from django.db import connection
from setup.settings import URL_LDP

class TypeAnalytics:

    TABLE = 'tabela'
    CLASSIFICATION =  'classificacao'
    BASKETS = 'cestinhas'
    STATISTIC = 'estatisticas'

@decorators.api_view(['GET'])
@decorators.schema(None)
@decorators.parser_classes([parsers.JSONParser])
def list_team_analytics(request,categoriaid,type:str,date):

    print(type)

    cob: Campeonato = Campeonato.objects.filter(datainicio__year=date,categoriaid=categoriaid)

    if cob.exists():

        cob = cob.first()

        if type.lower() == TypeAnalytics.BASKETS:
            rankPlayer = Equipejogadorjogo.objects.select_related('equipejogadorid','jogoid','posicaoid')
            rankPlayer = rankPlayer.values(id=F('equipejogadorid__jogadorid'),team=F('equipejogadorid__equipeid__nome'),name=F('equipejogadorid__jogadorid__nome'),lastName=F('equipejogadorid__jogadorid__sobrenome'),avatar=F('equipejogadorid__equipeid__urlbrasao'),photo=F('equipejogadorid__jogadorid__urlfoto'),teamPhoto=F('equipejogadorid__equipeid__urlfoto'))
            rankPlayer = rankPlayer.annotate(points=Sum('pontos'),p3=Sum('a3c'),p2=Sum('a2c'),p1=Sum('llc'),games=Count('jogoid'))
            rankPlayer = rankPlayer.filter(jogoid__campeonatoid=cob.id,jogoid__data__year=date)
            rankPlayer = rankPlayer.order_by('-points')
            
            # rankPlayer = EquipejogadorjogoSerializer(rankPlayer.all(),many=True)
            print(str(rankPlayer.query))

            return response.Response(rankPlayer.values())


        if type.lower() == TypeAnalytics.TABLE:
            games = Jogo.objects.select_related('equipeaid','equipebid')
            games = games.filter(campeonatoid=cob.id)
            games = games.filter(data__year=date)
            games = games.order_by('data')

            games = JogoSerializer(games.all(),many=True)
            
            return response.Response(games.data)

        if type.lower() == TypeAnalytics.CLASSIFICATION:
            with connection.cursor() as cursor:
                cursor.execute('''select Equipe.Id, 
                                  Nome, 
                                  concat(%s,Equipe.UrlBrasao) as photo,
                                  count(jogo.Id) as Jogos,
                                  (count(jogo.Id) * 2) - (count(jogo.Id) - sum(case when ResultadoFinalA > ResultadoFinalB and EquipeAId = equipe.Id then 1 when ResultadoFinalB > ResultadoFinalA and EquipeBId = equipe.Id then 1 else 0 end)) as points,
                                  ceil((sum(case when ResultadoFinalA > ResultadoFinalB and EquipeAId = equipe.Id then 1 when ResultadoFinalB > ResultadoFinalA and EquipeBId = equipe.Id then 1 else 0 end) / count(jogo.Id) * 100)) as porcentagem,
                                  sum(case when ResultadoFinalA > ResultadoFinalB and EquipeAId = equipe.Id then 1 when ResultadoFinalB > ResultadoFinalA and EquipeBId = equipe.Id then 1 else 0 end) as victory,
                                  count(jogo.Id) - sum(case when ResultadoFinalA > ResultadoFinalB and EquipeAId = equipe.Id then 1 when ResultadoFinalB > ResultadoFinalA and EquipeBId = equipe.Id then 1 else 0 end) as derrota,
                                  sum(case when EquipeAId = equipe.Id then ResultadoFinalA when EquipeBId = equipe.Id then ResultadoFinalB else 0 end) as Contra,
                                  sum( case when EquipeAId = equipe.Id then ResultadoFinalA when EquipeBId = equipe.Id then ResultadoFinalB else 0 end) -
                                  sum(case when EquipeAId != equipe.Id then ResultadoFinalA when EquipeBId != equipe.Id then ResultadoFinalB else 0 end) as CContra,
                                  sum(case when EquipeAId != equipe.Id then ResultadoFinalA when EquipeBId != equipe.Id then ResultadoFinalB else 0 end) as Saldo
                                  from jogo inner join equipe on equipe.Id = jogo.EquipeAId or equipe.Id = jogo.EquipeBId
                                  where CampeonatoId = %s and EstadoJogoId = 4 group by equipe.Id order by -sum(case
                                  when ResultadoFinalA > ResultadoFinalB and EquipeAId = equipe.Id then 1
                                   when ResultadoFinalB > ResultadoFinalA and EquipeBId = equipe.Id then 1
                                  else 0
                                  end);''',[URL_LDP,cob.id])

                classfications = cursor.fetchall()

                classfications = [{ 'id': data[0],
                                    'nameTeam': data[1],
                                    'avatar': data[2],
                                    'games': data[3],
                                    'points': data[4],
                                    'percentage': data[5],
                                    'victory': data[6],
                                    'defeat': data[7], 
                                    'against': data[8],
                                    'tookAgainst': data[9],
                                    'balance': data[10],
                                     } for data in classfications]

                return response.Response(classfications)

        if type.lower() == TypeAnalytics.STATISTIC:

            statistic = Equipejogadorjogo.objects.select_related('equipejogadorid','equipejogadorid__jogadorid','equipejogadorid__equipeid','jogoid')
            statistic = statistic.values(id=F('equipejogadorid__jogadorid'),equipeid=F('equipejogadorid__equipeid'),avatar=F('equipejogadorid__equipeid__urlbrasao'),nameTeam=F('equipejogadorid__equipeid__nome'),namePlayer=F('equipejogadorid__jogadorid__nome'),lastNamePlayer=F('equipejogadorid__jogadorid__sobrenome'))
            statistic = statistic.annotate(p=Sum('faltasp'),t=Sum('faltast'),u=Sum('faltasu'),d=Sum('faltasd'),points=Sum('pontos'),p3=Sum('a3c'),p2=Sum('a2c'),p1=Sum('llc'),total=Sum('faltasp') + Sum('faltast') + Sum('faltasu') + Sum('faltasd'),games=Count('jogoid'))
            statistic = statistic.values_list('id','equipeid','avatar','nameTeam','equipejogadorid__jogadorid__id','namePlayer','lastNamePlayer','games','equipejogadorid__equipeid__nome','p','t','u','d','total','p3','p2','p1','points')
            statistic = statistic.filter(jogoid__campeonatoid=cob.id,jogoid__data__year=date)
            statistic = statistic.order_by('-equipejogadorid__equipeid')

            print(str(statistic.query))

            return response.Response(statistic.values())

    return response.Response({ 'message' : "Not Found"},status=status.HTTP_400_BAD_REQUEST)




@decorators.api_view(['GET'])
@decorators.schema(None)
@decorators.parser_classes([parsers.JSONParser])
def teams_list(request):

    def team_all_and_serializer(team,serializer):         
        return { str(team.first().categoriaid.nome) : [serializer(t).data for t in team.all()] }

    def team_data_or_null(categoryId: int):
        team = Equipe.objects.filter(categoriaid=categoryId,sistema=0)
        return team if team.exists() else NULL

    def team_f(team):
        return True if team is not NULL else False

    return response.Response([team_all_and_serializer(teams,EquipeSerializer)
                                    for teams in filter(team_f,
                                        map(team_data_or_null,
                                            [category.id for category in Categoria.objects.all()]))])


@decorators.api_view(['GET'])
@decorators.schema(None)
@decorators.parser_classes([parsers.JSONParser])
def players_list_at_team(request,categoryid,teamid):

    equipejogadorquery = Equipejogador.objects.select_related('equipeid').filter(equipeid=teamid,equipeid__categoriaid=categoryid)
    equipejogadorall = EquipejogadorSerializer(equipejogadorquery.all(),many=True)

    return response.Response(equipejogadorall.data)