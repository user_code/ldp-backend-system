from ctypes.wintypes import HLOCAL
import json
from Dashboard.models import Equipejogador, Equipejogadorjogo, Campeonato, Jogador
from Dashboard.serializer import EquipejogadorRow, EquipejogadorjogoSerializer, JogadorSerializer
from django.shortcuts import get_object_or_404
from rest_framework import generics, viewsets, generics, pagination, permissions, pagination, filters, decorators, response, parsers
import django_filters

from django.db.models import Q, F, Func, Count, Sum, Avg
from django.core.serializers.json import DjangoJSONEncoder


class PaginationPlayerRank(pagination.PageNumberPagination):
    page_size = 5

class PlayersRank(generics.ListCreateAPIView):

    queryset = Equipejogador.objects.all()
    serializer_class = EquipejogadorRow
    
    filter_backends = [filters.OrderingFilter]
    ordering_fields = '__all__'

    pagination_class = PaginationPlayerRank

    def get_object():
        queryset = self.queryset
        return get_object_or_404(queryset)


class EquipeJogadorJogo(generics.ListCreateAPIView):

    queryset = Equipejogadorjogo.objects.select_related('equipejogadorid','jogoid','posicaoid')
    serializer_class = EquipejogadorjogoSerializer

    filter_backends = [filters.OrderingFilter]
    ordering_fields = '__all__'

    pagination_class = PaginationPlayerRank

    def get_object():
        queryset = self.queryset
        return get_object_or_404(queryset)


@decorators.api_view(['GET'])
@decorators.schema(None)
@decorators.parser_classes([parsers.JSONParser])
def rank_players_championship(request):

    campeonatos = Campeonato.objects.filter(datainicio__year=2022).order_by('-id')

    playersRank = []

    for campeonato in campeonatos:
        rankPlayer = Equipejogadorjogo.objects.all()
        rankPlayer = rankPlayer.values(id=F('equipejogadorid__jogadorid')).annotate(points=Sum('pontos'))
        rankPlayer = rankPlayer.filter(jogoid__campeonatoid=int(campeonato.id))
        rankPlayer = rankPlayer.order_by('-points')
        # playersRank.append(list(rankPlayer)[0])
        if len(rankPlayer):
            obPlayer = list(rankPlayer).pop(0)
            id = obPlayer['id']
            player = Jogador.objects.filter(id=id)[0]
            player = JogadorSerializer(player).data
            player.update({ 'pontos':  obPlayer['points'] })
            playersRank.append(player)

    return response.Response(playersRank)