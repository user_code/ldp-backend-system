from dataclasses import fields
import io
from Dashboard.serializer import JogoSerializer
from Dashboard.models import Jogo
from django.shortcuts import get_object_or_404, get_list_or_404
from Dashboard.filters import FilterNextGames
from rest_framework import viewsets, views, generics, response, pagination, permissions, filters, decorators, parsers
import django_filters

class PaginationNextGames(pagination.PageNumberPagination):
    page_size_query_param  = 'page_size'
    max_page_size = 100

class NextGames(generics.ListCreateAPIView):
    queryset = Jogo.objects.select_related('equipeaid','equipebid')
    serializer_class = JogoSerializer
    pagination_class = PaginationNextGames
    # filter_backends = [filters.OrderingFilter]
    
    filter_backends = (filters.OrderingFilter,django_filters.rest_framework.DjangoFilterBackend,)
    filterset_class = FilterNextGames
    ordering_fields = ('id','data','campeonatoid')
    # filter_fields = ('estadojogoid',)
    
    def get_object(self):
        # print(self.get_queryset())
        # print(queryset)
        # print(self.multiple_lookup_fields)
        queryset = self.queryset
        return get_object_or_404(queryset)



@decorators.api_view(['GET'])
@decorators.schema(None)
@decorators.parser_classes([parsers.JSONParser])
def next_games(request):
    
    return response.Response({ })


