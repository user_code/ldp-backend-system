from dataclasses import fields
import django_filters
from django_filters import rest_framework as filters
from Dashboard.models import Jogo, Estadojogo, Noticia

class FilterNextGames(django_filters.rest_framework.FilterSet):

    # estadojogoid = django_filters.rest_framework.RelatedFilter(Estadojogo,name='estado',looup_expr='iexact')
    data = django_filters.rest_framework.DateFilter(lookup_expr=('gt'))

    class Meta:
        model = Jogo
        fields = ['estadojogoid__id','estadojogoid__nome','data']


class FilterNoticie(django_filters.rest_framework.FilterSet):

    year = django_filters.rest_framework.NumberFilter(field_name='datapublicacao',lookup_expr='year')

    class Meta:
        model = Noticia
        fields = ['year']