from django.contrib import admin
from Dashboard.models import Categoria, Equipe, Equipeintegrantecomissao, Equipejogadorjogo, Estadojogo, Campeonato, Jogador, Jogo, Noticia, Noticiaitem, Tipodocumento, Cidade

# Register your models here.
admin.site.register(Categoria)
admin.site.register(Equipe)
admin.site.register(Estadojogo)
admin.site.register(Campeonato)
admin.site.register(Noticia)
admin.site.register(Noticiaitem)
admin.site.register(Jogo)
admin.site.register(Jogador)
admin.site.register(Cidade)

admin.site.register(Equipejogadorjogo)
admin.site.register(Equipeintegrantecomissao)
admin.site.register(Tipodocumento)