# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Aspnetroles(models.Model):
    id = models.CharField(db_column='Id', primary_key=True, max_length=128)  # Field name made lowercase.
    name = models.CharField(db_column='Name', unique=True, max_length=196)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AspNetRoles'


class Aspnetuserclaims(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    userid = models.ForeignKey('Aspnetusers', models.DO_NOTHING, db_column='UserId')  # Field name made lowercase.
    claimtype = models.TextField(db_column='ClaimType', blank=True, null=True)  # Field name made lowercase.
    claimvalue = models.TextField(db_column='ClaimValue', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AspNetUserClaims'


class Aspnetuserlogins(models.Model):
    loginprovider = models.CharField(db_column='LoginProvider', primary_key=True, max_length=128)  # Field name made lowercase.
    providerkey = models.CharField(db_column='ProviderKey', max_length=128)  # Field name made lowercase.
    userid = models.ForeignKey('Aspnetusers', models.DO_NOTHING, db_column='UserId')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AspNetUserLogins'
        unique_together = (('loginprovider', 'providerkey', 'userid'),)


class Aspnetuserroles(models.Model):
    userid = models.OneToOneField('Aspnetusers', models.DO_NOTHING, db_column='UserId', primary_key=True)  # Field name made lowercase.
    roleid = models.ForeignKey(Aspnetroles, models.DO_NOTHING, db_column='RoleId')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AspNetUserRoles'
        unique_together = (('userid', 'roleid'),)


class Aspnetusers(models.Model):
    id = models.CharField(db_column='Id', primary_key=True, max_length=128)  # Field name made lowercase.
    nome = models.TextField(db_column='Nome')  # Field name made lowercase.
    sobrenome = models.TextField(db_column='Sobrenome', blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='Email', max_length=256, blank=True, null=True)  # Field name made lowercase.
    emailconfirmed = models.IntegerField(db_column='EmailConfirmed')  # Field name made lowercase.
    passwordhash = models.TextField(db_column='PasswordHash', blank=True, null=True)  # Field name made lowercase.
    securitystamp = models.TextField(db_column='SecurityStamp', blank=True, null=True)  # Field name made lowercase.
    phonenumber = models.TextField(db_column='PhoneNumber', blank=True, null=True)  # Field name made lowercase.
    phonenumberconfirmed = models.IntegerField(db_column='PhoneNumberConfirmed')  # Field name made lowercase.
    twofactorenabled = models.IntegerField(db_column='TwoFactorEnabled')  # Field name made lowercase.
    lockoutenddateutc = models.DateTimeField(db_column='LockoutEndDateUtc', blank=True, null=True)  # Field name made lowercase.
    lockoutenabled = models.IntegerField(db_column='LockoutEnabled')  # Field name made lowercase.
    accessfailedcount = models.IntegerField(db_column='AccessFailedCount')  # Field name made lowercase.
    username = models.CharField(db_column='UserName', unique=True, max_length=196)  # Field name made lowercase.
    rg = models.TextField(db_column='RG', blank=True, null=True)  # Field name made lowercase.
    telefone = models.TextField(db_column='Telefone', blank=True, null=True)  # Field name made lowercase.
    urlfoto = models.TextField(db_column='UrlFoto', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AspNetUsers'


class Campeonato(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    categoriaid = models.ForeignKey('Categoria', models.DO_NOTHING, db_column='CategoriaId')  # Field name made lowercase.
    nome = models.CharField(db_column='Nome', max_length=256)  # Field name made lowercase.
    descricao = models.TextField(db_column='Descricao')  # Field name made lowercase.
    datainicio = models.DateTimeField(db_column='DataInicio')  # Field name made lowercase.
    datafim = models.DateTimeField(db_column='DataFim', blank=True, null=True)  # Field name made lowercase.
    localid = models.ForeignKey('Local', models.DO_NOTHING, db_column='LocalId', blank=True, null=True)  # Field name made lowercase.
    estadocampeonatoid = models.ForeignKey('Estadocampeonato', models.DO_NOTHING, db_column='EstadoCampeonatoId')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Campeonato'


class Categoria(models.Model):
    id = models.IntegerField(db_column='Id', primary_key=True)  # Field name made lowercase.
    nome = models.CharField(db_column='Nome', max_length=128)  # Field name made lowercase.
    nomeabreviado = models.CharField(db_column='NomeAbreviado', max_length=8)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Categoria'


class Cidade(models.Model):
    id = models.IntegerField(db_column='Id', primary_key=True)  # Field name made lowercase.
    nome = models.CharField(db_column='Nome', max_length=256)  # Field name made lowercase.
    estado = models.CharField(db_column='Estado', max_length=2)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Cidade'


class Documento(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    tipodocumentoid = models.ForeignKey('Tipodocumento', models.DO_NOTHING, db_column='TipoDocumentoId')  # Field name made lowercase.
    nome = models.CharField(db_column='Nome', max_length=512)  # Field name made lowercase.
    url = models.CharField(db_column='Url', max_length=1024)  # Field name made lowercase.
    criacaoregistro = models.DateTimeField(db_column='CriacaoRegistro')  # Field name made lowercase.
    ativo = models.IntegerField(db_column='Ativo')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Documento'


class Equipe(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    categoriaid = models.ForeignKey(Categoria, models.DO_NOTHING, db_column='CategoriaId')  # Field name made lowercase.
    cidadeid = models.ForeignKey(Cidade, models.DO_NOTHING, db_column='CidadeId')  # Field name made lowercase.
    nome = models.CharField(db_column='Nome', max_length=256)  # Field name made lowercase.
    nomeabreviado = models.CharField(db_column='NomeAbreviado', max_length=3)  # Field name made lowercase.
    descricao = models.TextField(db_column='Descricao')  # Field name made lowercase.
    urlbrasao = models.CharField(db_column='UrlBrasao', max_length=512, blank=True, null=True)  # Field name made lowercase.
    urlfoto = models.CharField(db_column='UrlFoto', max_length=512, blank=True, null=True)  # Field name made lowercase.
    ativo = models.IntegerField(db_column='Ativo')  # Field name made lowercase.
    tecnico = models.CharField(db_column='Tecnico', max_length=256, blank=True, null=True)  # Field name made lowercase.
    urltecnicofoto = models.CharField(db_column='UrlTecnicoFoto', max_length=512, blank=True, null=True)  # Field name made lowercase.
    assistente = models.CharField(db_column='Assistente', max_length=256, blank=True, null=True)  # Field name made lowercase.
    urlassistentefoto = models.CharField(db_column='UrlAssistenteFoto', max_length=512, blank=True, null=True)  # Field name made lowercase.
    medico = models.CharField(db_column='Medico', max_length=256, blank=True, null=True)  # Field name made lowercase.
    urlmedicofoto = models.CharField(db_column='UrlMedicoFoto', max_length=512, blank=True, null=True)  # Field name made lowercase.
    fisioterapeuta = models.CharField(db_column='Fisioterapeuta', max_length=256, blank=True, null=True)  # Field name made lowercase.
    urlfisioterapeutafoto = models.CharField(db_column='UrlFisioterapeutaFoto', max_length=512, blank=True, null=True)  # Field name made lowercase.
    massagista = models.CharField(db_column='Massagista', max_length=256, blank=True, null=True)  # Field name made lowercase.
    urlmassagistafoto = models.CharField(db_column='UrlMassagistaFoto', max_length=512, blank=True, null=True)  # Field name made lowercase.
    responsavelcontato = models.CharField(db_column='ResponsavelContato', max_length=256, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='Email', max_length=256, blank=True, null=True)  # Field name made lowercase.
    website = models.CharField(db_column='Website', max_length=256, blank=True, null=True)  # Field name made lowercase.
    telefone = models.CharField(db_column='Telefone', max_length=20, blank=True, null=True)  # Field name made lowercase.
    endereco = models.CharField(db_column='Endereco', max_length=512, blank=True, null=True)  # Field name made lowercase.
    enderecoginasio = models.CharField(db_column='EnderecoGinasio', max_length=512, blank=True, null=True)  # Field name made lowercase.
    sistema = models.IntegerField(db_column='Sistema', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Equipe'



class Equipeintegrantecomissao(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    equipeid = models.ForeignKey(Equipe, models.DO_NOTHING, db_column='EquipeId')  # Field name made lowercase.
    tipointegranteid = models.IntegerField(db_column='TipoIntegranteId')  # Field name made lowercase.
    nome = models.TextField(db_column='Nome')  # Field name made lowercase.
    urlfoto = models.TextField(db_column='UrlFoto', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'EquipeIntegranteComissao'


class Jogador(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    nome = models.CharField(db_column='Nome', max_length=256)  # Field name made lowercase.
    sobrenome = models.CharField(db_column='Sobrenome', max_length=512)  # Field name made lowercase.
    datanascimento = models.DateTimeField(db_column='DataNascimento', blank=True, null=True)  # Field name made lowercase.
    cpf = models.CharField(db_column='CPF', max_length=20, blank=True, null=True)  # Field name made lowercase.
    rg = models.CharField(db_column='RG', max_length=20, blank=True, null=True)  # Field name made lowercase.
    posicaoid = models.ForeignKey('Posicao', models.DO_NOTHING, db_column='PosicaoId', blank=True, null=True)  # Field name made lowercase.
    numero = models.IntegerField(db_column='Numero', blank=True, null=True)  # Field name made lowercase.
    urlfoto = models.CharField(db_column='UrlFoto', max_length=512, blank=True, null=True)  # Field name made lowercase.
    ativo = models.IntegerField(db_column='Ativo')  # Field name made lowercase.
    numeroregistro = models.CharField(db_column='NumeroRegistro', max_length=50, blank=True, null=True)  # Field name made lowercase.
    numeroregistrofederacao = models.CharField(db_column='NumeroRegistroFederacao', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Jogador'


class Equipejogador(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    equipeid = models.ForeignKey(Equipe, models.DO_NOTHING, db_column='EquipeId')  # Field name made lowercase.
    jogadorid = models.ForeignKey(Jogador, models.DO_NOTHING,related_name='jogadores',db_column='JogadorId')  # Field name made lowercase.
    numero = models.IntegerField(db_column='Numero', blank=True, null=True)  # Field name made lowercase.
    posicaoid = models.ForeignKey('Posicao', models.DO_NOTHING, db_column='PosicaoId', blank=True, null=True)  # Field name made lowercase.
    dataentrada = models.DateTimeField(db_column='DataEntrada')  # Field name made lowercase.
    datasaida = models.DateTimeField(db_column='DataSaida', blank=True, null=True)  # Field name made lowercase.
    ativo = models.IntegerField(db_column='Ativo')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'EquipeJogador'

class Estadojogo(models.Model):
    id = models.IntegerField(db_column='Id', primary_key=True)  # Field name made lowercase.
    nome = models.CharField(db_column='Nome', max_length=128)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'EstadoJogo'

class Jogo(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    codigo = models.CharField(db_column='Codigo', max_length=20)  # Field name made lowercase.
    campeonatoid = models.ForeignKey(Campeonato, models.DO_NOTHING, db_column='CampeonatoId')  # Field name made lowercase.
    data = models.DateTimeField(db_column='Data')  # Field name made lowercase.
    localid = models.ForeignKey('Local', models.DO_NOTHING, db_column='LocalId', blank=True, null=True)  # Field name made lowercase.
    equipeaid = models.ForeignKey(Equipe, models.DO_NOTHING, db_column='EquipeAId',related_name='equipeaid_equipe')  # Field name made lowercase.
    equipebid = models.ForeignKey(Equipe, models.DO_NOTHING, db_column='EquipeBId')  # Field name made lowercase.
    resultadofinala = models.IntegerField(db_column='ResultadoFinalA')  # Field name made lowercase.
    resultadofinalb = models.IntegerField(db_column='ResultadoFinalB')  # Field name made lowercase.
    estadojogoid = models.ForeignKey(Estadojogo, models.DO_NOTHING, db_column='EstadoJogoId')  # Field name made lowercase.
    walkover = models.IntegerField(db_column='Walkover')  # Field name made lowercase.
    pontosajusteresultadoa = models.IntegerField(db_column='PontosAjusteResultadoA')  # Field name made lowercase.
    pontosajusteresultadob = models.IntegerField(db_column='PontosAjusteResultadoB')  # Field name made lowercase.
    etapaid = models.IntegerField(db_column='EtapaId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Jogo'

class Equipejogadorjogo(models.Model):
    equipejogadorid = models.OneToOneField(Equipejogador, models.DO_NOTHING, related_name='equipes', db_column='EquipeJogadorId', primary_key=True)  # Field name made lowercase.
    jogoid = models.ForeignKey(Jogo, models.DO_NOTHING,related_name='equipejogos',db_column='JogoId')  # Field name made lowercase.
    numero = models.IntegerField(db_column='Numero')  # Field name made lowercase.
    posicaoid = models.ForeignKey('Posicao', models.DO_NOTHING, db_column='PosicaoId', blank=True, null=True)  # Field name made lowercase.
    pontos = models.IntegerField(db_column='Pontos')  # Field name made lowercase.
    a3c = models.IntegerField(db_column='A3C')  # Field name made lowercase.
    a2c = models.IntegerField(db_column='A2C')  # Field name made lowercase.
    llc = models.IntegerField(db_column='LLC')  # Field name made lowercase.
    faltasp = models.IntegerField(db_column='FaltasP')  # Field name made lowercase.
    faltast = models.IntegerField(db_column='FaltasT')  # Field name made lowercase.
    faltasu = models.IntegerField(db_column='FaltasU')  # Field name made lowercase.
    faltasd = models.IntegerField(db_column='FaltasD')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'EquipeJogadorJogo'
        unique_together = (('equipejogadorid', 'jogoid'),)

    @property
    def equipejogadorid_posicaoid(self):
        return self.equipejogadorid.jogadorid


class Equipepatrocinio(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    equipeid = models.ForeignKey(Equipe, models.DO_NOTHING, db_column='EquipeId')  # Field name made lowercase.
    nome = models.CharField(db_column='Nome', max_length=256)  # Field name made lowercase.
    urllogo = models.CharField(db_column='UrlLogo', max_length=512, blank=True, null=True)  # Field name made lowercase.
    ativo = models.IntegerField(db_column='Ativo')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'EquipePatrocinio'


class Escala(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    jogoid = models.ForeignKey('Jogo', models.DO_NOTHING, db_column='JogoId', blank=True, null=True)  # Field name made lowercase.
    local = models.CharField(db_column='Local', max_length=256, blank=True, null=True)  # Field name made lowercase.
    arbitroid = models.ForeignKey(Aspnetusers, models.DO_NOTHING, db_column='ArbitroId',related_name='arbitroid_aspnetusers')  # Field name made lowercase.
    fiscal1id = models.ForeignKey(Aspnetusers, models.DO_NOTHING, db_column='Fiscal1Id',related_name='fiscal1id_aspnetusers')  # Field name made lowercase.
    fiscal2id = models.ForeignKey(Aspnetusers, models.DO_NOTHING, db_column='Fiscal2Id', blank=True, null=True,related_name='fiscal2id_aspnetusers')  # Field name made lowercase.
    apontadorid = models.ForeignKey(Aspnetusers, models.DO_NOTHING, db_column='ApontadorId',related_name='apontadorid_aspnetusers')  # Field name made lowercase.
    operador24id = models.ForeignKey(Aspnetusers, models.DO_NOTHING, db_column='Operador24Id',related_name='operador24id_aspnetusers')  # Field name made lowercase.
    cronometristaid = models.ForeignKey(Aspnetusers, models.DO_NOTHING, db_column='CronometristaId',related_name='cronometristaid_aspnetusers')  # Field name made lowercase.
    representanteid = models.ForeignKey(Aspnetusers, models.DO_NOTHING, db_column='RepresentanteId')  # Field name made lowercase.
    localhorariosaida = models.CharField(db_column='LocalHorarioSaida', max_length=256, blank=True, null=True)  # Field name made lowercase.
    carro = models.CharField(db_column='Carro', max_length=256, blank=True, null=True)  # Field name made lowercase.
    criacaoregistro = models.DateTimeField(db_column='CriacaoRegistro', blank=True, null=True)  # Field name made lowercase.
    alteracaoregistro = models.DateTimeField(db_column='AlteracaoRegistro', blank=True, null=True)  # Field name made lowercase.
    codigo = models.TextField(db_column='Codigo', blank=True, null=True)  # Field name made lowercase.
    data = models.DateTimeField(db_column='Data', blank=True, null=True)  # Field name made lowercase.
    categoria = models.TextField(db_column='Categoria', blank=True, null=True)  # Field name made lowercase.
    equipea = models.TextField(db_column='EquipeA', blank=True, null=True)  # Field name made lowercase.
    equipeb = models.TextField(db_column='EquipeB', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Escala'


class Estadocampeonato(models.Model):
    id = models.IntegerField(db_column='Id', primary_key=True)  # Field name made lowercase.
    nome = models.CharField(db_column='Nome', max_length=128)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'EstadoCampeonato'



class Etapa(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    campeonatoid = models.ForeignKey(Campeonato, models.DO_NOTHING, db_column='CampeonatoId')  # Field name made lowercase.
    nome = models.TextField(db_column='Nome')  # Field name made lowercase.
    descricao = models.TextField(db_column='Descricao', blank=True, null=True)  # Field name made lowercase.
    datainicio = models.DateTimeField(db_column='DataInicio')  # Field name made lowercase.
    datafim = models.DateTimeField(db_column='DataFim', blank=True, null=True)  # Field name made lowercase.
    inscricaoaberta = models.IntegerField(db_column='InscricaoAberta')  # Field name made lowercase.
    estadoetapaid = models.ForeignKey(Estadocampeonato, models.DO_NOTHING, db_column='EstadoEtapaId')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Etapa'



class Local(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    nome = models.CharField(db_column='Nome', max_length=256)  # Field name made lowercase.
    logradouro = models.CharField(db_column='Logradouro', max_length=1024)  # Field name made lowercase.
    numero = models.IntegerField(db_column='Numero', blank=True, null=True)  # Field name made lowercase.
    cep = models.CharField(db_column='Cep', max_length=8, blank=True, null=True)  # Field name made lowercase.
    cidadeid = models.ForeignKey(Cidade, models.DO_NOTHING, db_column='CidadeId')  # Field name made lowercase.
    latitude = models.DecimalField(db_column='Latitude', max_digits=18, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    longitude = models.DecimalField(db_column='Longitude', max_digits=18, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    ativo = models.IntegerField(db_column='Ativo')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Local'


class Log(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    date = models.DateTimeField(db_column='Date')  # Field name made lowercase.
    thread = models.CharField(db_column='Thread', max_length=255)  # Field name made lowercase.
    level = models.CharField(db_column='Level', max_length=50)  # Field name made lowercase.
    logger = models.CharField(db_column='Logger', max_length=255)  # Field name made lowercase.
    message = models.CharField(db_column='Message', max_length=4000)  # Field name made lowercase.
    exception = models.CharField(db_column='Exception', max_length=2000, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Log'


class Noticia(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    datapublicacao = models.DateTimeField(db_column='DataPublicacao')  # Field name made lowercase.
    titulo = models.CharField(db_column='Titulo', max_length=256)  # Field name made lowercase.
    urlfoto = models.CharField(db_column='UrlFoto', max_length=512, blank=True, null=True)  # Field name made lowercase.
    texto = models.TextField(db_column='Texto')  # Field name made lowercase.
    publicadorid = models.ForeignKey(Aspnetusers, models.DO_NOTHING, db_column='PublicadorId')  # Field name made lowercase.
    esconder = models.IntegerField(db_column='Esconder')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Noticia'


class Noticiaitem(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    noticiaid = models.ForeignKey(Noticia, models.DO_NOTHING, db_column='NoticiaId')  # Field name made lowercase.
    noticiaitemtipoid = models.IntegerField(db_column='NoticiaItemTipoId')  # Field name made lowercase.
    url = models.CharField(db_column='Url', max_length=512)  # Field name made lowercase.
    urlthumb = models.CharField(db_column='UrlThumb', max_length=512, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NoticiaItem'


class Posicao(models.Model):
    id = models.IntegerField(db_column='Id', primary_key=True)  # Field name made lowercase.
    nome = models.CharField(db_column='Nome', max_length=128)  # Field name made lowercase.
    sigla = models.CharField(db_column='Sigla', max_length=2)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Posicao'


class Selecaorodada(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    noticiaid = models.ForeignKey(Noticia, models.DO_NOTHING, db_column='NoticiaId')  # Field name made lowercase.
    posicao1id = models.ForeignKey(Equipejogador, models.DO_NOTHING, db_column='Posicao1Id',related_name='posicao1id_equipejogador')  # Field name made lowercase.
    posicao2id = models.ForeignKey(Equipejogador, models.DO_NOTHING, db_column='Posicao2Id',related_name='posicao2id_equipejogador')  # Field name made lowercase.
    posicao3id = models.ForeignKey(Equipejogador, models.DO_NOTHING, db_column='Posicao3Id',related_name='posicao3id_equipejogador')  # Field name made lowercase.
    posicao4id = models.ForeignKey(Equipejogador, models.DO_NOTHING, db_column='Posicao4Id',related_name='posicao4id_equipejogador')  # Field name made lowercase.
    posicao5id = models.ForeignKey(Equipejogador, models.DO_NOTHING, db_column='Posicao5Id',related_name='posicao5id_equipejogador')  # Field name made lowercase.
    tecnicoid = models.ForeignKey(Equipeintegrantecomissao, models.DO_NOTHING, db_column='TecnicoId')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SelecaoRodada'


class Tipodocumento(models.Model):
    id = models.IntegerField(db_column='Id', primary_key=True)  # Field name made lowercase.
    nome = models.CharField(db_column='Nome', max_length=256)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'TipoDocumento'


class Migrationhistory(models.Model):
    migrationid = models.CharField(db_column='MigrationId', primary_key=True, max_length=150)  # Field name made lowercase.
    contextkey = models.CharField(db_column='ContextKey', max_length=300)  # Field name made lowercase.
    model = models.TextField(db_column='Model')  # Field name made lowercase.
    productversion = models.CharField(db_column='ProductVersion', max_length=32)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = '__MigrationHistory'


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'
