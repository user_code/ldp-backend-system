import random
from django.shortcuts import get_object_or_404, get_list_or_404

from Dashboard.serializer import NoticieSerializer
from Dashboard.filters import FilterNoticie
from Dashboard.models import Noticia

from rest_framework import generics, response, pagination, filters, decorators, parsers
from django_filters import rest_framework as filters
import django_filters


class PaginationNoticie(pagination.PageNumberPagination):
    page_size_query_param = 'page_size'
    page_query_param = 'page'
    max_page_size = 20

class FilterNoticieModel(filters.FilterSet):

    date_range = filters.DateRangeFilter(field_name='datapublicacao')
    year = filters.NumberFilter(field_name='datapublicacao',lookup_expr='year')
    exclude_title = filters.BaseInFilter(field_name='titulo',exclude=True)

    order_by = filters.OrderingFilter(
        fields = (
            ('id','id')
        )
    )

    class Meta:
        model = Noticia
        fields = ['date_range','id','exclude_title']

class Noticies(generics.ListCreateAPIView):
    
    queryset = Noticia.objects.all()
    serializer_class = NoticieSerializer
    pagination_class = PaginationNoticie

    filterset_class = FilterNoticieModel

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = self.filter_queryset(queryset)

        print(self.filter_queryset)

        if 'random' in self.request.query_params:
             queryset = queryset.order_by('?')

        print(queryset)
        return queryset