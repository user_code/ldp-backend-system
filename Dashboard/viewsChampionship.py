from Dashboard.serializer import Categoria
from Dashboard.models import Campeonato, Categoria
from rest_framework import generics, decorators, parsers, response, views, viewsets

from django.db import connection

@decorators.api_view(['GET'])
@decorators.schema(None)
@decorators.parser_classes([parsers.JSONParser])
def championship_menu(request):

    categorys = Categoria.objects.all().values()
    types = [ "Tabela", "Classificação", "Cestinhas", "Estatísticas" ]
    
    years = []
    with connection.cursor() as cursor:
        cursor.execute('select year(DataInicio) from Campeonato group by year(DataInicio) order by -year(DataInicio);')
        result = cursor.fetchall()
        years = [ item[0] for item in result]
        cursor.close()

    return response.Response({ "categorys": categorys, "types": types, "years": years })