from dataclasses import fields
from datetime import date
from email.headerregistry import UniqueAddressHeader
from pyexpat import model
from rest_framework import routers, serializers, viewsets
from Dashboard.models import Categoria, Equipe, Equipejogador, Equipejogadorjogo, Jogador, Jogo, Noticia, Posicao
from setup.settings import URL_LDP

class CategoriaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Categoria
        fields = '__all__'

class EquipeSerializer(serializers.ModelSerializer):

    categoria = CategoriaSerializer(source="categoriaid")
    teamPhoto = serializers.SerializerMethodField('get_format_urlfoto')
    avatar = serializers.SerializerMethodField('get_format_urlbrasao')
    

    class Meta:
        model = Equipe
        fields = '__all__'

        
    def get_format_urlfoto(self,obj):
        return "{0}{1}".format(URL_LDP,obj.urlfoto)

    def get_format_urlbrasao(self,obj):
        return "{0}{1}".format(URL_LDP,obj.urlbrasao)


days_off = ["Segunda","Terça","Quarta","Quinta","Sexta","Sábado","Domingo"]

class JogoSerializer(serializers.ModelSerializer):

    equipeA = EquipeSerializer(source="equipeaid")
    equipeB = EquipeSerializer(source="equipebid")

    date = serializers.SerializerMethodField('get_game_date')
    hour = serializers.SerializerMethodField('get_game_hour')
    day = serializers.SerializerMethodField('get_game_day')

    class Meta:
        model = Jogo
        fields = '__all__'

    def get_game_date(self,obj) -> str:
        return obj.data.strftime('%d/%m')

    def get_game_hour(self,obj) -> str:
        return obj.data.strftime('%H:%M')

    def get_game_day(self,obj) -> str : 
        return days_off[obj.data.weekday()]

class PosicaoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Posicao
        fields = '__all__'


class JogadorSerializer(serializers.ModelSerializer):

    avatar = serializers.SerializerMethodField('get_photo_url')

    class Meta:
        model = Jogador
        fields = '__all__'

    def get_photo_url(self,obj):
        return "{0}{1}".format(URL_LDP,obj.urlfoto) 


class EquipejogadorSerializer(serializers.ModelSerializer):

    jogador = JogadorSerializer(source="jogadorid")
    equipe = EquipeSerializer(source='equipeid')

    class Meta:
        model = Equipejogador
        fields = '__all__'


class EquipejogadorRow(serializers.ModelSerializer):

    equipes = serializers.ReadOnlyField()

    class Meta:
        model = Equipejogador
        fields = ('id','equipeid','jogadorid','equipes')

class EquipejogadorjogoSerializer(serializers.ModelSerializer):

    #equipes = serializers.HyperlinkedRelatedField(many=True,view_name='equipe-detail',queryset=Equipejogador.objects.all())
    equipeJogador = EquipejogadorSerializer(source='equipejogadorid')
    jogo = JogoSerializer(source='jogoid')
    posicao = PosicaoSerializer(source='posicaoid')

    class Meta:
        model = Equipejogadorjogo
        fields = '__all__'

class EquipejogadorjogoBaseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Equipejogadorjogo
        fields = '__all__'

class EquipeJogadorjogoRowSerializer(serializers.ModelSerializer):

    equipejogador = EquipejogadorSerializer()

    class Meta:
        model = Equipejogadorjogo
        fields = '__all__'


class NoticieSerializer(serializers.ModelSerializer):

    photo = serializers.SerializerMethodField('get_url_photo')

    class Meta:
        model = Noticia
        fields = '__all__'

    def get_url_photo(self,obj):
        return "{0}{1}".format(URL_LDP,obj.urlfoto)
            