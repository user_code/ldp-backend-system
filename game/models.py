from pyexpat import model
from django.db import models

# Create your models here.
class Game(models.Model):

    name = models.CharField(max_length=30)
    today = models.DateField()

    def __str__(self) -> str:
        return self.name


class Team(models.Model):

    category = (
        ('Sub17','Masculino'),
        ('Sub17','Femenino')
    )

    categoryLevel = models.CharField(max_length=6,choices=category,blank=False,null=False,default='Sub17')
    

class Versus(models.Model):

    teamLeft = models.ForeignKey(Game,on_delete=models.CASCADE)
    teamRight = models.ForeignKey(Team,on_delete=models.CASCADE)