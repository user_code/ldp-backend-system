from rest_framework import routers, serializers, viewsets
from rest_framework.permissions import IsAuthenticated

from game.models import Game, Team, Versus
from game.serializer import GameSerializer, TeamSerializer, VersusSerializer

# Create your views here.

class GameViewSet(viewsets.ModelViewSet):

    queryset = Game.objects.all()
    serializer_class = GameSerializer
    permission_classes = (IsAuthenticated,)

class TeamViewSet(viewsets.ModelViewSet):

    queryset = Team.objects.all()
    serializer_class = TeamSerializer
    permission_classes = (IsAuthenticated,)

class VersusViewSet(viewsets.ModelViewSet):

    queryset = Versus.objects.all()
    serializer_class = VersusSerializer