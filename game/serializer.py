from dataclasses import field
from rest_framework import routers, serializers, viewsets
from game.models import Game, Team, Versus

class GameSerializer(serializers.ModelSerializer):

    class Meta:
        model = Game
        fields = ['id','name','today']


class TeamSerializer(serializers.ModelSerializer):

    class Meta:
        model = Team
        fields = '__all__'

class VersusSerializer(serializers.ModelSerializer):

    class Meta:
        model = Versus
        exclude = []