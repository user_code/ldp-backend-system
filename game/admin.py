from django.contrib import admin
from game.models import Game, Team, Versus
# Register your models here.

class Games(admin.ModelAdmin):
    list_display = ('id','name','today')
    list_display_links = ('id','name')
    search_fields = ('name',)
    list_per_page = 20

admin.site.register(Game,Games)

class Teams(admin.ModelAdmin):
    list_display = ('id','categoryLevel')
    list_display_links = ['id']
    search_fields = ('categoryLevel',)
    list_per_page = 20
    

admin.site.register(Team,Teams)

class VersusAdmin(admin.ModelAdmin):

    list_display = ('id','teamLeft','teamRight')
    list_display_links = ['id']
    list_per_page: int = 20


admin.site.register(Versus,VersusAdmin)